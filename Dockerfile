FROM swaggerapi/swagger-codegen-cli-v3:latest

RUN set -x && \
    apk add --no-cache git
    
ENTRYPOINT []